$(document).ready(function() {
    moreFunds();
    if ($('.bs-slider').length != 0) {
        slider();
    }
    if ($('.bs-invest-swiper').length != 0) {
        investSwiper();
    }
    if ($('.js-datepicker').length != 0) {
        datepicker();
    }
    if ($('.js-modal').length != 0) {
        modal();
    }
    sip();
    fundsEdit();
    selectPicker();
    tabShow();
    riskSwiper();
    riskSlider();
    profileBtn();
    radioInvest();
    accordion();
});

var questionSLider;
jQuery(document).click(function(e) {
    e.stopPropagation();
    jQuery('.js-drop-menu').removeClass('open');
});

function moreFunds() {
    $('.action-button .btn-more').click(function() {
        var $this = $(this);
        $this.parents('.bs-sip-detail').find('.fund-list').toggleClass('active');
        $this.toggleClass('active');
        if ($this.hasClass('active')) {
            $this.text('Show Less');
        } else {
            $this.text('See More Funds ');
        }
    });
}

function selectPicker() {
    jQuery('.bs-select .input-group').click(function(e) {
        e.stopPropagation();
        if (jQuery(this).closest('.bs-select').hasClass('open') == false) {
            jQuery(this).closest('.bs-select').addClass('open');
            jQuery(this).closest('.bs-select').find('.select-option').slideDown();
        } else {
            jQuery(this).closest('.bs-select').removeClass('open');
            jQuery(this).closest('.bs-select').find('.select-option').slideUp();
        }
    });
    jQuery('.bs-select .select-option .option').click(function(e) {
        e.stopPropagation();
        var selectedValue = jQuery(this).html();
        jQuery(this).closest('.bs-select').find('.form-control').val(selectedValue);
        jQuery('.bs-select .select-option').slideUp();
        jQuery('.bs-select').removeClass('open');
    });
}

function fundsEdit() {
    jQuery('.js-fund-edit').click(function() {
        // var scrollPosition = jQuery(document).scrollTop();
        var obj = jQuery(this);
        if (jQuery(this).hasClass('edited') == true) {
            jQuery(this).removeClass('edited');
            if (jQuery(this).closest('.js-form-slide').hasClass('js-edit') == true) {
                jQuery(this).html('Edit');
            } else if (jQuery(this).closest('.js-form-slide').hasClass('js-invest') == true) {
                jQuery(this).html('Invest Now');
            }
            jQuery(this).closest('.js-form-slide').find('.bs-form').css('opacity', '0');
            setTimeout(function() {
                obj.closest('.js-form-slide').find('.js-hidden').slideUp();
            }, 200);
        } else {
            jQuery(this).addClass('edited');
            jQuery(this).html('Close');
            jQuery(this).closest('.js-form-slide').find('.js-hidden').slideDown();
            setTimeout(function() {
                obj.closest('.js-form-slide').find('.bs-form').css('opacity', '1');
            }, 200);
            jQuery('html,body').animate({
                scrollTop: obj.closest('.js-form-slide').offset().top
            }, 500);
        }
    })
    jQuery('.js-fund-save').click(function() {
        var obj = jQuery(this);
        jQuery(this).closest('.js-form-slide').find('.js-fund-edit').removeClass('edited');
        if (jQuery(this).closest('.js-form-slide').hasClass('js-edit') == true) {
            jQuery(this).closest('.js-form-slide').find('.js-fund-edit').html('Edit');
        } else if (jQuery(this).closest('.js-form-slide').hasClass('js-invest') == true) {
            jQuery(this).closest('.js-form-slide').find('.js-fund-edit').html('Invest Now');
        }
        jQuery(this).closest('.js-form-slide').find('.bs-form').css('opacity', '0');
        setTimeout(function() {
            obj.closest('.js-form-slide').find('.js-hidden').slideUp();
        }, 200);
    })
    jQuery('.js-fund-delete').click(function() {
        jQuery(this).closest('.js-form-slide').remove();
    });
}

function tabShow() {
    $('.js-tab').click(function() {
        var currTarget = $(this).data('id');
        $('.js-tab').removeClass('active');
        $(this).addClass('active');
        $('.js-tab-show').hide();
        $('#' + currTarget).show();
        if ($('#' + currTarget).hasClass('mob-slide') && $(window).width() < 1024) {
            $('html,body').animate({
                scrollTop: $('#' + currTarget).offset().top
            }, 1200);
        }
    })
}

function sip() {
    $('.js-invest').click(function() {
        var investAmt = $('.js-invest:checked').val();
        $(this).closest('.bs-sip-detail').find('.form-control.amount').val(investAmt);
    });

    $('.js-graph .radio-wrap input[type=radio]').click(function() {
        var currTarget = $('.js-graph .radio-wrap').find('input[type=radio]:checked').attr('id');
        $(this).closest('.js-graph').find('.graph-detail .graph').removeClass('active');
        $('.graph[data-id=' + currTarget + ']').addClass('active');
    });

    /* fund listing */

    $('.js-radio input[type=radio]').click(function() {
        var radioVal = $(this).closest('.form-wrap').find('.js-hideshow');
        console.log(radioVal);
        if ($(this).hasClass('js-radio-click')) {
            radioVal.css("display", "none");

        } else {
            radioVal.css("display", "inline-block");
        }
    });
}

function investSwiper() {
    if ($(window).width() > 768) {
        var mySwiper = new Swiper('.bs-invest-swiper .swiper-container', {
            slidesPerView: 1,
            spaceBetween: 50,
            direction: 'vertical',
            pagination: '.swiper-pagination',
            paginationClickable: true,
            paginationBulletRender: function(swiper, index, text) {
                return '<span class="' + text + '"><span class="text">' + (index == 0 ? 'Popular Investments' : index == 1 ? 'Popular Investments 2' : 'Popular Investments 3') + '</span></span>';
            }
        });
    }
}

function slider() {
    $('.num-char').keypress(function(e) {
        var x = e.which || e.keycode;
        if ((x >= 48 && x <= 57) || x == 8 ||
            (x >= 35 && x <= 40) || x == 46)
            return true;
        else
            return false;
    });
    $('.num-char').change(function() {
        var numVal = parseInt($(this).val());
        $(this).closest('.form-group').find('.bs-slider')[0].noUiSlider.set([numVal]);
    });
    var stepSlider = document.getElementById('range');
    noUiSlider.create(stepSlider, {
        start: [0],
        step: 1,
        connect: [true, false],
        range: {
            'min': 0,
            'max': 500000
        },
        pips: {
            mode: 'values',
            values: [0, 500000],
            format: {
                to: function(value) {
                    return value.toString().charAt(0) + ' lac';
                },
                from: function(value) {
                    return value.replace('lac', '');
                }
            }
        }
    });
    stepSlider.noUiSlider.on('update', function(values, handle) {
        var val = Math.round(values[handle]);
        $($(this)[0].target).closest('.form-group').find('input').val(val.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
    });
    var stepSlider1 = document.getElementById('range1');
    noUiSlider.create(stepSlider1, {
        start: [0],
        step: 1,
        connect: [true, false],
        range: {
            'min': [0],
            'max': [30]
        },
        pips: {
            mode: 'values',
            values: [0, 30],
            format: {
                to: function(value) {
                    return value + '%';
                },
                from: function(value) {
                    return value.replace('%', '');
                }
            }
        }

    });
    stepSlider1.noUiSlider.on('update', function(values, handle) {
        var val = Math.round(values[handle]);
        $($(this)[0].target).closest('.form-group').find('input').val(val);
    });
    var stepSlider2 = document.getElementById('range2');
    noUiSlider.create(stepSlider2, {
        start: [0],
        step: 1,
        connect: [true, false],
        range: {
            'min': [0],
            'max': [30]
        },
        pips: {
            mode: 'values',
            values: [0, 30],
            format: {
                to: function(value) {
                    return value + ' yrs';
                },
                from: function(value) {
                    return value.replace('yrs', '');
                }
            }
        }

    });
    stepSlider2.noUiSlider.on('update', function(values, handle) {
        var val = Math.round(values[handle]);
        $($(this)[0].target).closest('.form-group').find('input').val(val);
    });
}

function datepicker() {
    $(".js-datepicker").datepicker({
        // minDate: 0,
        beforeShowDay: function(date) {
            var highlightDate = [22, 23, 26];
            for (i = 0; i < highlightDate.length; i++) {
                if (date.getDate() == highlightDate[i]) {
                    return [true, 'highlight'];
                }
            }
            return [false, ''];
        },
        onClose: function() {
            $('.cm-overlay').removeClass('active'), $("body").css("overflow", "initial");
        },
        beforeShow: function() {
            $('.cm-overlay').addClass('active'), $("body").css("overflow", "hidden");
        }
    });
    $('.edit-link').click(function() {
        $(this).closest('.input-group').find(".js-datepicker").datepicker('show');
    });
}

function modal() {
    $('.js-modal').click(function() {
        var videoSrs = $(this).attr('data-src');
        $('.bs-modal.typ-video').find('iframe').attr('src', videoSrs);
        $('.bs-modal.typ-video').addClass('active'), $('.cm-overlay').addClass('active'), $("body").css("overflow", "hidden");
        $('.bs-modal').addClass('active'), $('.cm-overlay').addClass('active'), $("body").css("overflow", "hidden");
    });
    $('.cm-overlay').click(function() {
        $('.bs-modal.typ-video').removeClass('active'), $('.cm-overlay').removeClass('active'), $("body").css("overflow", "initial");
        $('.bs-modal').removeClass('active'), $('.cm-overlay').removeClass('active'), $("body").css("overflow", "initial");
        $('.bs-modal.typ-video').find('iframe').attr('src', '');

    });
    $('.close-btn').click(function() {
        $('.bs-modal.typ-video').removeClass('active'), $('.cm-overlay').removeClass('active'), $("body").css("overflow", "initial");
        $('.bs-modal').removeClass('active'), $('.cm-overlay').removeClass('active'), $("body").css("overflow", "initial");
        $('.bs-modal.typ-video').find('iframe').attr('src', '');

    })
}

function riskSwiper() {
    if (jQuery('.lyt-questionnaire').length != 0) {
        questionSLider = new Swiper('#questionSwiper', {
            slidesPerView: 'auto',
            spaceBetween: 8,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            simulateTouch: false,
            breakpoints: {
                767: {
                    centeredSlides: true
                }
            }
        });
    }
}

function riskSlider() {
    jQuery('.bs-question-card input[type=radio]').click(function() {
        if (jQuery(this).closest('.swiper-slide').hasClass('swiper-slide-active') == true) {
            // jQuery('.js-risk-next').click();
            questionSLider.slideNext();
            jQuery(this).closest('.swiper-slide').next('.swiper-slide').find('.bs-question-card').addClass('current');
        }
    })
}

function profileBtn() {
    jQuery('.js-drop-menu').click(function(e) {
        e.stopPropagation();
        jQuery('.js-menu').fadeOut();
        // jQuery(this).removeClass('open');
        var currTarget = jQuery(this).attr('id');
        if (jQuery(this).hasClass('open') == false) {
            jQuery('.js-drop-menu').removeClass('open');
            jQuery(this).addClass('open');
            jQuery('.js-menu[data-target=' + currTarget + ']').fadeIn();
        } else {
            jQuery(this).removeClass('open');
            jQuery('.js-menu[data-target=' + currTarget + ']').fadeOut();
        }
    })
}

function radioInvest() {
    jQuery('.bs-radio.typ-amt input').change(function() {
        if (jQuery(this)[0].checked == true) {
            var checkVal = jQuery(this).next('label').find('.value').html();
            jQuery(this).closest('.left-wrap').find('.form-group.typ-amt .form-control').val(checkVal);
            jQuery(this).closest('.form-item').find('.form-group.typ-amt .form-control').val(checkVal);
        }
    })
}

function accordion() {
    $(".bs-accordion .acc-title").click(function() {
        if ($(this).closest(".acc-item").hasClass("active")) {
            $(this).closest(".acc-item").removeClass("active");
        } else {
            $(".bs-accordion .acc-item").removeClass("active");
            $(this).closest(".acc-item").addClass("active");
        }
    });
}